# recognice

The flutter-based cross platform mobile presence of Recogn.Ice (recognice.org).

## Functionality

Let's you take pictures of glaciers and icebergs. The pictures are used for the recogn.ice project.


## Participate

Any issues found? Features you want to have? Improvements you want to make?

Feel free to contribute. Clone the project, submit issues, and develop the app even further. We are happy for any help we can get.


