import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';

// A screen that allows users to take a picture using a given camera.
class FinalPage extends StatefulWidget {
  // A widget that displays the picture taken by the user.
  // Stores and uploads image, or discards it

  const FinalPage({
    Key key,
  }) : super(key: key);

  @override
  _FinalPageState createState() => _FinalPageState();
}

class _FinalPageState extends State<FinalPage> {
  StreamSubscription<List<PurchaseDetails>> _subscription;
  Directory _mediaDir;
  bool _loggedIn;
  InAppPurchaseConnection _iap = InAppPurchaseConnection.instance;
  bool _available, _prodLoaded = false;
  List<ProductDetails> _products = [];
  List<PurchaseDetails> _purchases = [];
  int _credits = 0;

  final String gemID = 'iceBerg';

  Map _content;
  final Completer<WebViewController> _webController =
      Completer<WebViewController>();

  @override
  void initState() {
    _initializeProducts();
    getContent();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void getContent() async {
    Directory contentDir = await getApplicationDocumentsDirectory();
    File(contentDir.path + "/content/content.json")
        .readAsString()
        .then((cString) {
      Map content = jsonDecode(cString);
      setState(() {
        _content = content;
      });
    });
  }

  void _launchURL(url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Widget _parseContent(String content, double height, double width) {
    return Center(
      child: Stack(
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(8.0)),
            ),
            child: WebView(
              initialUrl: content,
              javascriptMode: JavascriptMode.unrestricted,
              onWebViewCreated: (WebViewController webViewController) {
                if (!_webController.isCompleted) {
                  _webController.complete(webViewController);
                }
              },
              navigationDelegate: (NavigationRequest request) {
                _launchURL(request.url);
                return NavigationDecision.prevent;
              },
            ),
            height: height,
            width: width,
          ),
          Container(
            height: height,
            width: width,
            child: GestureDetector(
              onTap: () => _launchURL(content),
            ),
          ),
        ],
      ),
    );
  }

  Future<bool> _onWillPop() {
    return showDialog(
          context: context,
          builder: (context) => new AlertDialog(
            title: new Text('Are you sure?'),
            content: new Text('Do you want to exit an App'),
            actions: <Widget>[
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text('No'),
              ),
              new FlatButton(
                onPressed: () => exit(0),
                child: new Text('Yes'),
              ),
            ],
          ),
        ) ??
        false;
  }

  void _initializeProducts() async {
    // Listen to new purchases
    _subscription = _iap.purchaseUpdatedStream.listen((data) => setState(() {
          print('NEW PURCHASE');
          _purchases.addAll(data);
          _verifyPurchase();
        }));

    _available = await _iap.isAvailable();
    if (_available) {
      await _getProducts();
      await _getPastPurchases();

      // Verify and deliver a purchase with your own business logic
      _verifyPurchase();
    }
  }

  // Get all products available for sale
  Future<void> _getProducts() async {
    Set<String> prodIds = Set.from([
      'icecube',
      'iceberg',
      'glacier',
      'northpole',
      'southpole',
    ]);
    Set<String> servIds = Set.from([
      'calving',
      'melting',
    ]);
    ProductDetailsResponse response = await _iap.queryProductDetails(prodIds);

    // Sort Price ascending
    List<ProductDetails> products = response.productDetails;
    products.sort((a, b) =>
        a.skuDetail.priceAmountMicros.compareTo(b.skuDetail.priceAmountMicros));
    setState(() {
      _products = products;
      _prodLoaded = true;
    });
  }

  /// Gets past purchases
  Future<void> _getPastPurchases() async {
    QueryPurchaseDetailsResponse response = await _iap.queryPastPurchases();

    for (PurchaseDetails purchase in response.pastPurchases) {
      if (Platform.isIOS) {
        InAppPurchaseConnection.instance.completePurchase(purchase);
      }
    }

    setState(() {
      _purchases = response.pastPurchases;
    });
    _spendCredits();
  }

  /// Returns purchase of specific product ID
  List<PurchaseDetails> _purchasesOf(String productID) {
    return _purchases
        .where((purchase) => purchase.productID == productID)
        .toList();
  }

  /// Returns purchase of specific product ID
  PurchaseDetails _hasPurchased(String productID) {
    return _purchases.firstWhere((purchase) => purchase.productID == productID,
        orElse: () => null);
  }

  /// Your own business logic to setup a consumable
  void _verifyPurchase() {
    PurchaseDetails purchase = _hasPurchased(gemID);

    // TODO serverside verification & record consumable in the database

    if (purchase != null && purchase.status == PurchaseStatus.purchased) {
      _credits = 10;
    }
  }

  /// Purchase a product
  void _buyProduct(ProductDetails prod) {
    final PurchaseParam purchaseParam = PurchaseParam(productDetails: prod);
    // _iap.buyNonConsumable(purchaseParam: purchaseParam);
    _iap.buyConsumable(purchaseParam: purchaseParam, autoConsume: true);
  }

  /// Spend credits and consume purchase when they run pit
  void _spendCredits() async {
    for (var purchase in _purchases) {
      // /// Decrement credits
      // setState(() {
      //   _credits--;
      // });

      // /// TODO update the state of the consumable to a backend database
      // // Mark consumed when credits run out
      // if (_credits == 0) {
      var res = await _iap.consumePurchase(purchase);
      print(res);
      // }
    }
  }

  Widget parseArticle(Map<String, dynamic> article, height, width) {
    double spacer = 35;
    if (article.containsKey('content')) {
      if (article.containsKey('height')) {
        height = article['height'].toDouble();
      }
      if (article.containsKey('width')) {
        width = article['width'].toDouble();
      }
      return (article.containsKey('title'))
          ? Card(
              child: ExpansionTile(
                  leading: Icon(Icons.web),
                  title: Text(
                    article['title'],
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  // dense: true,
                  children: <Widget>[
                    _parseContent(article['content'], height, width),
                    Container(
                      height: spacer * 2,
                      child: GestureDetector(
                        onTap: () => _launchURL(article['content']),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(Icons.chrome_reader_mode),
                            Container(width: spacer),
                            Text("Continue reading"),
                            //_launchURL(request.url);,
                          ],
                        ),
                      ),
                    ),
                  ]),
            )
          : _parseContent(article['content'], height, width);
    }
    Crashlytics.instance.setString('ParseOnlineContent',
        "Unable to parse online content: " + article.toString());
    return Container(
      child: Text("Unable to query object."),
    );
  }

  List<Widget> _purchaseWidget(BuildContext context) {
    double spacer = 15;
    bool paymentsActivated = !_prodLoaded || _products.length == 0 ||
        !_content.containsKey("payments") || !_content["payments"];
    return [
      Card(
        child: !_loggedIn
            ? ExpansionTile(
                leading: Icon(Icons.info),
                title: Text(
                  "You must login for more functionality",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                children: <Widget>[
                  Text("Click on the lock symbol in the lower right corner."),
                  Container(height: spacer),
                ],
              )
            : paymentsActivated
                ? ExpansionTile(
                    leading: Icon(Icons.monetization_on),
                    title: Text("Donate to Recogn.Ice",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    children: <Widget>[
                      Text("-- Comming Soon --\n"
                      "At the moment, we cannot accept donations.",
                      textAlign: TextAlign.center),
                      Container(height: spacer),
                    ],
                  )
                : ExpansionTile(
                    leading: Icon(Icons.monetization_on),
                    title: Text("Donate to Recogn.Ice",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    // dense: true,
                    children: <Widget>[
                      Text("We appreciate your donations.\nThis keeps our service running.",
                      textAlign: TextAlign.center,),
                      for (var prod in _products) ...[
                        Card(
                          child: ExpansionTile(
                            leading: Image.asset(
                              'assets/icons/icon.png',
                            ),
                            title: Text(
                              prod.title.replaceAll(' (Recogn.Ice)', ''),
                              // style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            children: <Widget>[
                              Text(
                                prod.description,
                                textAlign: TextAlign.center,
                                style: TextStyle(fontStyle: FontStyle.italic),
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  if (_hasPurchased(prod.id) != null)
                                    Row(
                                      children: <Widget>[
                                        Icon(Icons.add_shopping_cart),
                                        Text(
                                          ' ${_purchasesOf(prod.id).length}',
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                  Container(width: spacer),
                                  Text(prod.price),
                                  Container(width: spacer),
                                  FlatButton(
                                    child: (_hasPurchased(prod.id) != null)
                                        ? Text('Buy More')
                                        : Text('Buy It'),
                                    onPressed: () => {
                                      _buyProduct(prod),
                                    },
                                    color: Colors.lightBlueAccent,
                                    shape: RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(8.0),
                                      side: BorderSide(color: Colors.blueGrey),
                                    ),
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                      ]
                    ],
                  ),
      )
    ];
  }

  @override
  Widget build(BuildContext context) {
    Directory mediaDir = Provider.of<Directory>(context);
    setState(() {
      _mediaDir = mediaDir;
    });
    String userId = Provider.of<String>(context);
    bool loggedIn = Provider.of<bool>(context);
    setState(() {
      _loggedIn = loggedIn;
    });
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    String tooltip = 'Take another photo.';
    String route = '/capture_page';
    IconData icon = Icons.camera_alt;
    if (userId == null) {
      tooltip = 'Login for more.';
      route = '/signin_page';
      icon = Icons.lock_open;
    }

    return WillPopScope(
        onWillPop: _onWillPop,
        child: Scaffold(
          body: CustomScrollView(slivers: <Widget>[
            SliverAppBar(
              expandedHeight: height * 2 / 3,
              flexibleSpace: FlexibleSpaceBar(
                title: Text('Recogn.Ice'),
                centerTitle: true,
                background: Image.file(
                  File(mediaDir.path + '/wallpapers/wallpaper_2.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
              floating: false,
              pinned: true,
              snap: false,
            ),
            SliverList(
              delegate: SliverChildListDelegate(
                [
                  if (_content != null) ...[
                    if (_content.containsKey('payments')) ...[
                      for (int i = 0; i < _purchaseWidget(context).length; i++)
                        _purchaseWidget(context).elementAt(i),
                      Image.file(
                        File(mediaDir.path + '/wallpapers/wallpaper_3.jpg'),
                        fit: BoxFit.cover,
                      ),
                    ],
                    if (_content.containsKey('articles')) ...[
                      for (int i = 0; i < _content['articles'].length; i++) ...[
                        if (_content['articles'][i].containsKey('active') &&
                            _content['articles'][i]['active']) ...[
                          parseArticle(_content['articles'][i], height, width),
                          Image.file(
                            File(mediaDir.path +
                                '/wallpapers/wallpaper_${4 + i}.jpg'),
                            fit: BoxFit.cover,
                          ),
                        ],
                      ],
                    ],
                  ],
                ],
              ),
            ),
          ]),
          floatingActionButton: FloatingActionButton(
            tooltip: tooltip,
            child: Icon(icon),
            onPressed: () => Navigator.of(context).pushReplacementNamed(route),
          ),
        ));
  }
}
