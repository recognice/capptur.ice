import 'dart:async';
import 'dart:io';

import 'package:RecognIce/processing_page.dart';
import 'package:camera/camera.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_compass/flutter_compass.dart';
import 'package:geolocator/geolocator.dart';
import 'package:native_device_orientation/native_device_orientation.dart';
import 'package:path/path.dart' show join;
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:wakelock/wakelock.dart';

// A screen that allows users to take a picture using a given camera.
class CaptureGlacierPage extends StatefulWidget {
  CaptureGlacierPage({Key key}) : super(key: key);

  @override
  TakePicturePageState createState() => TakePicturePageState();
}

class TakePicturePageState extends State<CaptureGlacierPage> {
  List<CameraDescription> cameras;
  CameraController _controller;
  Future<void> _initializeControllerFuture;
  int selectedCamera = 0;
  bool isReady = false;
  double controlSize = 100;
  Geolocator geolocator = Geolocator();
  Position _myCurrentPosition;
  StreamSubscription<Position> _positionStreamSubscription;
  StreamSubscription _compass;
  double _myBearing;

  void _orientatePhone() {
    if (_positionStreamSubscription == null) {
      const LocationOptions locationOptions =
          LocationOptions(accuracy: LocationAccuracy.best, distanceFilter: 1);
      final Stream<Position> positionStream =
          geolocator.getPositionStream(locationOptions);
      _positionStreamSubscription = positionStream.listen(
          (Position position) => setState(() => _myCurrentPosition = position));
    }

    _positionStreamSubscription.onData(
        (Position position) => setState(() => _myCurrentPosition = position));

    _compass = FlutterCompass.events.listen((double direction) {
      setState(() {
        _myBearing = direction;
      });
    });
  }

  Future<void> _setupCamera() async {
    try {
      // initialize cameras.
      cameras = await availableCameras();

      debugPrint("Finished initializing cameras.");

      // initialize camera controllers.
      _controller = new CameraController(
          cameras[selectedCamera], ResolutionPreset.ultraHigh);

      await _controller.initialize();
    } on CameraException catch (error) {
      debugPrint("Some error occured!");
      debugPrint(error.toString());
    } on RangeError catch (error) {
      debugPrint("Unable to find any camera");
      debugPrint(error.toString());
    }

    if (!mounted) {
      return;
    }

    setState(() {
      isReady = true;
    });
  }

  Future<bool> _onWillPop() {
    return showDialog(
          context: context,
          builder: (context) => new AlertDialog(
            title: new Text('Are you sure?'),
            content: new Text('Do you want to exit an App'),
            actions: <Widget>[
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text('No'),
              ),
              new FlatButton(
                onPressed: () => exit(0),
                child: new Text('Yes'),
              ),
            ],
          ),
        ) ??
        false;
  }

  @override
  void initState() {
    super.initState();
    // To display the current output from the Camera,
    // create a CameraController.
    _setupCamera();
    _orientatePhone();
    Wakelock.enable();
  }

  @override
  void dispose() {
    // Dispose of the controller when the widget is disposed.
    if (_positionStreamSubscription != null) {
      _positionStreamSubscription.cancel();
      _positionStreamSubscription = null;
    }
    Wakelock.isEnabled.then((onEnabled) {
      if (onEnabled) {
        Wakelock.disable();
      }
    });
    _controller.dispose();
    _compass.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            child: _controller != null && _controller.value.isInitialized
                ? camerapreview(context)
                : Center(
                    child: Container(
                        height: 120,
                        width: 120,
                        child: CircularProgressIndicator())),
          ),
          Container(
            child: FutureBuilder<GeolocationStatus>(
                future: Geolocator().checkGeolocationPermissionStatus(),
                builder: (BuildContext context,
                    AsyncSnapshot<GeolocationStatus> snapshot) {
                  if (!snapshot.hasData) {
                    return Center(
                        child: Container(
                            width: 250,
                            height: 250,
                            child: CircularProgressIndicator()));
                  }
                  return Container();
                }),
          ),
          Positioned(
            top: - 0.1*height,
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height * 0.9,
            child: _controller != null && _controller.value.isInitialized
                ? Center(
                    child: NativeDeviceOrientationReader(
                        useSensor: true,
                        builder: (context) {
                          NativeDeviceOrientation orientation =
                              NativeDeviceOrientationReader.orientation(
                                  context);
                          int turns;
                          switch (orientation) {
                            case NativeDeviceOrientation.landscapeLeft:
                              turns = 1;
                              break;
                            case NativeDeviceOrientation.landscapeRight:
                              turns = -1;
                              break;
                            case NativeDeviceOrientation.portraitDown:
                              turns = 2;
                              break;
                            default:
                              turns = 0;
                              break;
                          }
                          return RotatedBox(
                            quarterTurns: turns,
                            child: Image.asset('assets/Frame_Empty.png',
                                fit: BoxFit.contain),
                          );
                        }))
                : new Container(),
          ),
          Positioned(
            bottom: height * 0.1,
            left: MediaQuery.of(context).size.width * 0.5 - controlSize / 2,
            child: Center(
              child: Container(
                height: controlSize,
                width: controlSize,
                child: cameracontrols(context),
              ),
            ),
          ),
          //For debugging the location
          // Container(
          //   child: Text(_myCurrentPosition.toString()),
          // ),
          // TODO: add change camera functionality
          // Positioned(
          //   top: MediaQuery.of(context).size.height*0.1,
          //   left: MediaQuery.of(context).size.width*0.5 - controlSize/2,
          //   child:Center(
          //     child: Container(
          //       height: controlSize,
          //       width: controlSize,
          //       child: cameraswap(context),
          //     ),
          //   ),
          // ),
        ],
      ),
    );
    // });
  }

  Widget camerapreview(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    double deviceRatio = width / height;
    double controllerRatio = _controller.value.aspectRatio;
    double scaleFactor =_controller.value.aspectRatio / deviceRatio;

    // Slim phone layout -> height is set by phone, width is calculated
    if (deviceRatio > controllerRatio) {
      // Wide phone layout -> width is set by phone, height is calculated
      scaleFactor = 1 / scaleFactor;
    }
    return Scaffold(
      body:  Transform.scale(
          scale: scaleFactor,
          child: Center(
            child: AspectRatio(
              aspectRatio: _controller.value.aspectRatio,
              child: CameraPreview(_controller),
            ),
          ),
      ),
    );
  }

  Widget cameracontrols(BuildContext context) {
    return Stack(
      children: <Widget>[
        // Opacity(
        //   opacity: 0.31415/2,
        //   child: Container(color: Colors.grey[100]),
        // ),
        FlatButton(
            child: Image.asset(
              'assets/icons/icon.png',
              width: controlSize,
              height: controlSize,
            ),
            // Provide an onPressed callback.
            onPressed: () async {
              // Take the Picture in a try / catch block. If anything goes wrong,
              // catch the error.
              try {
                // Ensure that the camera is initialized.
                await _initializeControllerFuture;

                // Construct the path where the image should be saved using the
                // pattern package.
                final path = join(
                  // Store the picture in the temp directory.
                  // Find the temp directory using the `path_provider` plugin.
                  (await getTemporaryDirectory()).path,
                  '${DateTime.now()}.png',
                );

                // Attempt to take a picture and log where it's been saved.
                await _controller.takePicture(path);

                _sendPhotoCaptured();
                // If the picture was taken, display it on a new screen.
                // Navigator.of(context).pushReplacementNamed("/processing_page");
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ProcessingPage(
                        imagePath: path,
                        location: _myCurrentPosition,
                        bearing: _myBearing),
                  ),
                );
              } catch (e) {
                // If an error occurs, log the error to the console.
                print(e);
              }
            }),
      ],
    );
  }

  Future<void> _sendPhotoCaptured() async {
    FirebaseAnalytics analytics = Provider.of<FirebaseAnalytics>(context);
    await analytics.logEvent(
      name: "PhotoCaptured",
      parameters: <String, dynamic>{
        'string': "Photo was taken.",
      },
    );
  }

// Widget cameraswap(BuildContext context){
//   return FloatingActionButton(
//     child: Icon(Icons.camera_rear),
//     onPressed: () async{
//       try{
//         List cameras = await availableCameras();
//         for (CameraDescription camera in cameras){
//           if (camera.name != widget.cameraController.name){
//             //widget.cameraController = camera;
//           }
//         }
//       } catch (e) {
//         // If an error occurs, log the error to the console.
//         print(e);
//       }
//     },
//   );
// }
}
