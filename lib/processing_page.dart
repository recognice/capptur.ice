import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:gallery_saver/gallery_saver.dart';
import 'package:geolocator/geolocator.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:share/share.dart';
import 'package:uuid/uuid.dart';

// A screen that allows users to take a picture using a given camera.
class ProcessingPage extends StatefulWidget {
  // A widget that displays the picture taken by the user.
  // Stores and uploads image, or discards it

  ProcessingPage(
      {Key key,
      this.app,
      @required this.imagePath,
      this.location,
      this.bearing})
      : super(key: key);

  final FirebaseApp app;
  final String imagePath;
  final Position location;
  final double bearing;

  @override
  _ProcessingPageState createState() =>
      _ProcessingPageState(imagePath, location, bearing);
}

class _ProcessingPageState extends State<ProcessingPage> {
  _ProcessingPageState(this.imagePath, this.location, this.bearing);

  FirebaseApp myApp;
  FirebaseStorage _myStorage;
  FirebaseDatabase _myDatabase;

  final imagePath;
  final location;
  final bearing;
  final timestamp = DateTime.now().millisecondsSinceEpoch;

  @override
  void initState() {
    this.myApp = widget.app;
    this._myDatabase = FirebaseDatabase(
      app: myApp,
      databaseURL: 'https://recognice-1554146422028.firebaseio.com',
    );
    this._myStorage = FirebaseStorage(
        app: myApp, storageBucket: 'gs://recognice-1554146422028.appspot.com');

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _willPop(BuildContext context){
    Navigator.pushReplacementNamed(context, '/capture_page');
  }

  Future<bool> _discardChanges(BuildContext context) async {
    _willPop(context);
    // Delete Picture
    bool success = false;
    File(imagePath).delete().then((value){
      return true;
    });
    return success;
  }

  // Widget disclaimer(BuildContext context){
  // }

  @override
  Widget build(BuildContext context) {
    Directory mediaDir = Provider.of<Directory>(context);
    var userId = Provider.of<String>(context);
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    var picBorderW = 0.01 * width;
    var picBorderH = 0.01 * height;

    return WillPopScope(
      onWillPop: (){
        return _discardChanges(context);
      }, //, '/capture_page'),
      child: Scaffold(
      body: Container(
        // color: Colors.lightBlue[200],
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Opacity(
              opacity: 0.3,
              child: Container(
                child: OverflowBox(
                  maxWidth: double.infinity,
                  alignment: Alignment.center,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      UnconstrainedBox(
                          constrainedAxis: Axis.vertical,
                          child: Image.file(
                              File(mediaDir.path +
                                  '/wallpapers/wallpaper_2.jpg'),
                              fit: BoxFit.cover))
                    ],
                  ),
                ),
              ),
            ),
            Positioned(
              top: picBorderH,
              left: picBorderW,
              height: height * 0.85,
              child: Container(
                alignment: Alignment.center,
                width: width - 2 * picBorderW,
                child: ClipRRect(
                  borderRadius: new BorderRadius.circular(8.0),
                  child: Image.file(
                    File(imagePath),
                    fit: BoxFit.fitWidth,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        child: Container(
          height: 80,
          child: Row(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                ),
                width: width / 2, // - 2 * picBorderW,
                child: Center(
                  child: Column(
                    children: <Widget>[
                      IconButton(
                        color: Colors.blueAccent,
                        icon: Icon(Icons.backspace),
                        tooltip: 'Discard Photo.',
                        onPressed: () => _discardChanges(context),
                      ),
                      GestureDetector(
                        behavior: HitTestBehavior.deferToChild,
                        onTap: () => _discardChanges(context),
                        child: Text('Discard',
                            style: TextStyle(color: Colors.blueAccent)),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                width: width / 2, // - 2 * picBorderW,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                ),
                child: Center(
                  child: Column(
                    children: <Widget>[
                      IconButton(
                        icon: Icon(Icons.send),
                        color: Colors.blueAccent,
                        tooltip: 'Store and continue.',
                        onPressed: () {
                          _storeFile(userId, imagePath);
                          Navigator.pushReplacementNamed(
                            context,
                            '/final_page',
                          );
                        },
                      ),
                      GestureDetector(
                        behavior: HitTestBehavior.opaque,
                        onTap: () {
                          _storeFile(userId, imagePath);
                          Navigator.pushReplacementNamed(
                            context,
                            '/final_page',
                          );
                        },
                        child: Text('Continue',
                            style: TextStyle(color: Colors.blueAccent)),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),

      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.share),
        tooltip: 'Share',
        onPressed: () {
          _shareImage(userId, imagePath, context);
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    ),);
  }

  void _shareImage(String userId, String path, BuildContext context) {

    Share.image(path: imagePath.toString(),
        title: "#Recogn.Ice",
        text:"Glacier or Iceberg? #Recogn.Ice");
    _storeFile(userId, imagePath);
    Navigator.pushReplacementNamed(
      context,
      '/final_page',
    );
  }

  void _storeFile(String userId, String path) async {
    Directory tmpDir = await getApplicationDocumentsDirectory();
    try {
      Uint8List image = await File(path).readAsBytes();
      String tmpFile = tmpDir.path + "/" + basename(path);
      await File(tmpFile).writeAsBytes(image);
      _uploadFile(userId, tmpFile);

      GallerySaver.saveImage(path, albumName: 'Pictures/Recogn.Ice')
          .then((bool success) {
        if (!success) {
          throw "Unable to store Picture in Gallery.";
        }
      });
    } catch (e) {
      Crashlytics.instance.setString("LocalPhotoStorage",
          "Unable to store photo locally: " + e.toString());
    }
  }

  void _uploadFile(String userId, String filePath) async {

    // Create uuid for the picture
    String fileID = new Uuid().v1().toString();

    // Path at the server
    String storagePath = 'pictures/' + userId + '/' + basename(fileID);

    StorageReference myStorageRef = _myStorage.ref().child(storagePath);
    StorageUploadTask uploadTask = myStorageRef.putFile(File(filePath));

    DatabaseReference myDbRef = _myDatabase.reference();

    double lat = location == null ? 0.0 : location.latitude;
    double lng = location == null ? 0.0 : location.longitude;

    // Create JSON data structure for database picture link
    Map<String, dynamic> picJSON = {
      fileID: {
        "location": {
          "lat": lat,
          "lng": lng,
        },
        "bearing": bearing,
        "timestamp": timestamp,
        "userid": userId,
        "file_location": storagePath,
      },
    };

    // Create JSON data structure for database user link
    Map<String, dynamic> userJSON = {
      fileID: {
        "file_location": storagePath,
      },
    };

    myDbRef.child("pictures/").update(picJSON);
    myDbRef.child('users/' + userId + "/pictures/").update(userJSON);

    StreamSubscription<StorageTaskEvent> streamSubscription =
        uploadTask.events.listen((event) {
        // Here, every StorageTaskEvent concerning the upload is printed to the logs.
          if (event.type == StorageTaskEventType.success){
            print('PictureUploadEvent: ${event.type}');
          }
    });

    // Cancel your subscription when done.
    await uploadTask.onComplete;
    streamSubscription.cancel();
    File(filePath).delete();
  }
}
