// Copyright 2019 Recognice.org. All rights reserved.

import 'dart:async';
import 'dart:collection';
import 'dart:io';
import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';

import 'package:RecognIce/boot_page.dart';
import 'package:RecognIce/capture_page.dart';
import 'package:RecognIce/final_page.dart';
import 'package:RecognIce/processing_page.dart';
import 'package:RecognIce/signin_page.dart';
import 'package:connectivity/connectivity.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/foundation.dart';
// import 'package:flutter_crashlytics/flutter_crashlytics.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';

main() async {

  // Required for Crashlytics initialization
  WidgetsFlutterBinding.ensureInitialized();

  FlutterError.onError = (FlutterErrorDetails details) {
    if (!kReleaseMode) {
      FlutterError.dumpErrorToConsole(details);
    } else {
      Zone.current.handleUncaughtError(details.exception, details.stack);
    }
  };

  // await FlutterCrashlytics().initialize();

  // runZoned<Future<Null>> (() async {
  runApp(RecogniceApp());
  // }, onError: (error, stackTrace) async {
  //   await FlutterCrashlytics().reportCrash(error, stackTrace, forceCrash: false);
  //   // TODO: get userInformation
  //   //await FlutterCrashlytics().setUserInfo(identifier, email, name).reportCrash(error, stackTrace, forceCrash: false);
  // });

}

class RecogniceApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => RecogniceAppState();
}

class RecogniceAppState extends State<RecogniceApp> {

  FirebaseApp _myApp;
  static FirebaseAnalytics analytics = FirebaseAnalytics();
  final FirebaseAnalyticsObserver observer =
      FirebaseAnalyticsObserver(analytics: analytics);
  String _userId;
  Directory _mediaDir;
  bool _isLoggedIn;

  @override
  void initState() {
    _setMediaDir();
    _setPortraitOrientation();
    _getLocalUser().then((_userId) {
      setState(() {
        this._userId = _userId;
      });
    });
    createApp();
    super.initState();
  }

  void _setMediaDir() async {
    try {
      await getApplicationDocumentsDirectory().then((mediaDir) {
        setState(() {
          _mediaDir = mediaDir;
        });
      });
      _getInAppMedia();
    } catch (e){
      print(e.toString());
    }
  }

  // Callback function is required for first usage of the app -->
  // no userId/navBarIndex is set yet --> provide callback to the following pages
  void _setUserId(String _userId) async {
    _storeUserLocal(_userId);
    setState(() {
      this._userId = _userId;
    });
    _getInAppMedia();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          Provider<String>.value(value: _userId),
          Provider<bool>.value(value: _isLoggedIn),
          Provider<Directory>.value(value: _mediaDir),
          Provider<FirebaseApp>.value(value: _myApp),
          Provider<FirebaseAnalytics>.value(value: analytics),
          Provider<FirebaseAnalyticsObserver>.value(value: observer),
        ],
        child: MaterialApp(
          theme: ThemeData(
            brightness: Brightness.light,
            primaryColor: Colors.blueAccent[100],
          ),
          title: 'Recogn.ice Authentication',
          home: BootPage(),
          routes: <String, WidgetBuilder>{
            "/boot_page": (BuildContext context) => new BootPage(),
            "/signin_page": (BuildContext context) => new SignInPage(
                  setUserId: _setUserId,
                ),
            "/capture_page": (BuildContext context) =>
            new CaptureGlacierPage(
              //positionStreamSubscription: _positionStreamSubscription,
            ),
            "/processing_page": (BuildContext context) => new ProcessingPage(
                  imagePath: null,
                  location: null,
                  bearing: null,
                ),
            "/final_page": (BuildContext context) => new FinalPage(),
          },
          navigatorObservers: <NavigatorObserver>[observer],
        ));
  }

  void _getInAppMedia() async {
    // TODO: Implement this routine. this is dependent on issue:
    // https://github.com/FirebaseExtended/flutterfire/issues/83
    if (_mediaDir == null){
      return;
    }

    // Store asset files to temp directory on the first boot
    String clipDir = _mediaDir.path + "/clips";
    String wallpaperDir = _mediaDir.path + "/wallpapers";
    String contentDir = _mediaDir.path + "/content";
    if (!await Directory(clipDir).exists()) {
      await Directory(clipDir).create();
    }
    if (!await Directory(wallpaperDir).exists()) {
      await Directory(wallpaperDir).create();
    }
    if (!await Directory(contentDir).exists()) {
      await Directory(contentDir).create();
    }

    if (!await File(contentDir + "/content.json").exists()) {
      ByteData assetFile = await rootBundle.load('assets/content/content.json');
      File localFile = new File('$contentDir/content.json');
      final buffer = assetFile.buffer;
      await localFile.writeAsBytes(
          buffer.asUint8List(assetFile.offsetInBytes, assetFile.lengthInBytes));
    }

    if (!await File(clipDir + "/boot_clip.mp4").exists()) {
      ByteData assetFile = await rootBundle.load('assets/clips/boot_clip.mp4');
      File localFile = new File('$clipDir/boot_clip.mp4');
      final buffer = assetFile.buffer;
      await localFile.writeAsBytes(
          buffer.asUint8List(assetFile.offsetInBytes, assetFile.lengthInBytes));
    }

    int numQueryPics = 8;
    for (int i=1; i<=numQueryPics; i++) {
      if (!await File("$wallpaperDir/wallpaper_$i.jpg").exists()) {
        ByteData assetFile =
        await rootBundle.load('assets/wallpapers/wallpaper_$i.jpg');
        File localFile = new File('$wallpaperDir/wallpaper_$i.jpg');
        final buffer = assetFile.buffer;
        await localFile.writeAsBytes(
            buffer.asUint8List(
                assetFile.offsetInBytes, assetFile.lengthInBytes));
      }
    }
    // Ensure a logged in user.
    if (!await _loggedIn()) {
      return;
    }

    // Queries required media from the server and replaces current assets
    FirebaseStorage myStorage = FirebaseStorage.instance;
    StorageReference ref =
        myStorage.ref().child('public_media').child('content');
    try {
      LinkedHashMap content = await ref.listAll();
      LinkedHashMap files = content['items'];
      for (int i = 0; i < files.length; i++) {
        final LinkedHashMap file = files[files.keys.elementAt(i)];
        final downloadFile = file['name'];
        final String url = await ref.child(downloadFile).getDownloadURL();
        final http.Response downloadData = await http.get(url);
        var bytes = downloadData.bodyBytes;
        File localFile = new File('$contentDir/$downloadFile');
        await localFile.writeAsBytes(bytes);
        String content = await File('$contentDir/$downloadFile')
            .readAsString();
        numQueryPics = jsonDecode(content)['articles'].length + 5;
      }
    } catch (e) {
      Crashlytics.instance.setString("MediaQuery", e.toString());
    }

    ref = myStorage.ref().child('public_media').child('wallpapers');
    try {
      // Get links to all  available files on the server
      LinkedHashMap content = await ref.listAll();
      LinkedHashMap files = content['items'];
      // Prepare for random querying
      final _random = new Random();
      List queriedPics = [];

      // prevent the while loop of running forever
      numQueryPics = min(numQueryPics, files.length);
      while (queriedPics.length < numQueryPics) {
        var item = _random.nextInt(files.length);
        if (!queriedPics.contains(item)) {
          queriedPics.add(item);
        }
      }
      for (int i = 0; i < numQueryPics; i++) {
        final LinkedHashMap file = files[files.keys.elementAt(queriedPics[i])];
        final downloadFile = file['name'];
        final String url = await ref.child(downloadFile).getDownloadURL();
        final http.Response downloadData = await http.get(url);
        var bytes = downloadData.bodyBytes;
        File localFile = new File('$wallpaperDir/wallpaper_$i.jpg');
        await localFile.writeAsBytes(bytes);
      }
    } catch (e) {
      Crashlytics.instance.setString("MediaQuery", e.toString());
    }

    // Check if new media download is actually required (reduce traffic)
    // if ((DateTime.now().millisecondsSinceEpoch -
    //     File(clipDir + "/boot_clip.mp4")
    //         .lastModifiedSync()
    //         .millisecondsSinceEpoch) <
    //     630000000) {
    //   // One week
    //   return;
    // }

    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.wifi) {
      ref = myStorage.ref().child('public_media').child('clips');
      try {
        LinkedHashMap content = await ref.listAll();
        LinkedHashMap files = content['items'];
        // generates a new Random object
        final _random = new Random();
        final LinkedHashMap file =
        files[files.keys.elementAt(_random.nextInt(files.length))];
        final downloadFile = file['name'];
        final String url = await ref.child(downloadFile).getDownloadURL();
        final http.Response downloadData = await http.get(url);
        var bytes = downloadData.bodyBytes;
        File localFile = new File('$clipDir/boot_clip.mp4');
        await localFile.writeAsBytes(bytes);
      } catch (e) {
        Crashlytics.instance.setString("MediaQuery", e.toString());
      }
    }
  }

  Future<String> _getLocalUser() async {
    // Create storage
    final secStorage = new FlutterSecureStorage();
    // Read all values
    Map<String, String> user = await secStorage.readAll();
    if (user != null && user.containsKey('userId')) {
      this._userId = user['userId'];
      if (await _loggedIn()) {
        print("User: " + user['userId']);
        return user['userId'];
      }
      Crashlytics.instance
          .setString('LocalUser', "Unable to query local user.");
    }
    return null;
  }

  Future<void> _storeUserLocal(_userId) async {
    // Create storage
    final storage = new FlutterSecureStorage();
    // Read value
    try {
      // Delete value
      await storage.delete(key: 'userId');
      print("Updating user credential.");
    } catch (e) {
      print("Couldn't delete User from internal storage. Creating new one.");
    }

    // Write value
    await storage.write(key: 'userId', value: _userId);
  }

  Future<bool> _loggedIn() async {
    FirebaseAuth _myAuth = FirebaseAuth.instance;
    FirebaseUser _user = await _myAuth.currentUser();
    if (_user == null) {
      Crashlytics.instance.setString('Recogn.IceLogin',
          "Failed to login. Potentially no Internet connection.");
      // else{ NOT YET LOGGED IN. Probably first time opening app. }
      setState(() {
        this._isLoggedIn = false;
      });
      return false;
    }
    setState(() {
      this._isLoggedIn = true;
    });
    return true;
  }

  void _setPortraitOrientation() async {
    await SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
  }

  void createApp() async {
    _myApp = await FirebaseApp.configure(
      name: 'RecognIce',
      options: FirebaseOptions(
        googleAppID: Platform.isIOS
            ? '1:119989404123:ios:7c353a224c0fbecf0c67fe'
            : '1:119989404123:android:0b45554779600c8f0c67fe',
        gcmSenderID: '159623150305',
        apiKey: 'AIzaSyAdA0itZQgMHKneBPZb8-WZkedHT3nXKrw',
        projectID: 'recognice-1554146422028',
      ),
    );
  }
}
