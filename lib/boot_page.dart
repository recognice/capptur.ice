import 'dart:io';

import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:video_player/video_player.dart';

class BootPage extends StatefulWidget {
  BootPage({
    Key key,
  }) : super(key: key);

  @override
  _BootPageState createState() => _BootPageState();
}

class _BootPageState extends State<BootPage> {
  VideoPlayerController _controller;
  double aspectRatio = 1.0;
  bool _videoReady = false;

  void _getController() async {
    String dir = (await getApplicationDocumentsDirectory()).path;
    _controller = VideoPlayerController.file(File('$dir/clips/boot_clip.mp4'))
      ..initialize().then((_) {
        setState(() {
          if (0.0 < _controller.value.aspectRatio) {
            aspectRatio = _controller.value.aspectRatio;
          }
        });
      });
    _controller.play();
    _controller.setLooping(true);
    _controller.setVolume(0);
    setState(() {
      _videoReady = true;
    });
  }

  @override
  void initState() {
    _getController();
    super.initState();
  }

  void onDispose(){
    _videoReady = false;
    _controller.pause();
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    String userId = Provider.of<String>(context);
    Directory mediaDir = Provider.of<Directory>(context);

    var navString = ' Capture.Ice';
    var route = '/capture_page';
    var icon = Icons.photo_camera;

    if (userId == null) {
      navString = ' Sign In';
      route = '/signin_page';
      icon = Icons.lock_open;
    }

    // Screen sizes
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Center(
            child: Container(
              width: width,
              height: height,
              child: OverflowBox(
                minWidth: width,
                minHeight: height,
                maxWidth: double.infinity,
                maxHeight: double.infinity,
                alignment: Alignment.center,
                // child: FittedBox(
                //   fit: BoxFit.cover,
                //   alignment: Alignment.center,
                child: Container(
                  height: height,
                  child: _videoReady
                      ? AspectRatio(
                          aspectRatio: aspectRatio,
                          child: _controller.value.initialized
                              ? VideoPlayer(
                                  _controller,
                                )
                              : CircularProgressIndicator(),
                        )
                      : Image.asset("assets/icons/recognice-logo.png"),
                ),
                //),
              ),
              // ),
            ),
          ),
          Center(
            child: Container(
              width: 200,
              height: 400,
              child: Column(
                children: <Widget>[
                  Center(
                    child: Container(
                      width: 190,
                      height: 80,
                      child: Stack(
                        children: <Widget>[
                          Opacity(
                            opacity: 0.31415, // Pi rocks
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.grey[100],
                                borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                              ),
                            ),
                          ),
                          Center(
                            child: mediaDir == null ? Container() : FlatButton(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Icon(
                                    icon,
                                    size: 32,
                                  ),
                                  Text(
                                    navString,
                                    style: TextStyle(fontSize: 15.0),
                                  ),
                                ],
                              ),
                              onPressed: () =>
                                  Navigator.pushReplacementNamed(context, route),
                            ),
                          ),
                        ],
                      ),
                      padding: const EdgeInsets.all(16),
                    ),
                  ),
                  Center(
                    child: Container(
                      width: 190,
                      height: 80,
                      child: Stack(
                        children: <Widget>[
                          Opacity(
                            opacity: 0.31415, // Pi rocks
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.grey[100],
                                borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                              ),
                            ),
                          ),
                          Center(
                            child: mediaDir == null ? Container() : FlatButton(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Icon(
                                    Icons.ac_unit,
                                    size: 32,
                                  ),
                                  Text(
                                    '    ... more',
                                    style: TextStyle(fontSize: 15.0),
                                  ),
                                ],
                              ),
                              onPressed: () =>
                                  Navigator.pushReplacementNamed(context,
                                      '/final_page'),
                            ),
                          ),
                        ],
                      ),
                      padding: const EdgeInsets.all(16),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
      // bottomNavigationBar: _navBar,
    );
  }
}
